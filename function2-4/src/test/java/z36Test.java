import static org.junit.jupiter.api.Assertions.*;

class intToStrTest {

    @org.junit.jupiter.api.Test
    void normal() throws Exception {
        double Mas = 8;
        String actual=Convert.doubleToStr(Mas);
        String expected="8.0";
        assertEquals(expected,actual);
    }


    @org.junit.jupiter.api.Test
    public void strToInt() {

            String number = "8";
            int actual=Convert.strToInt(number);
            int expected=8;
            assertEquals(expected,actual);

    }
    @org.junit.jupiter.api.Test
    public void strToDouble() {

        String numberdbl = "8.55";
        double actual=Convert.strToDouble(numberdbl);
        double expected=8.55;
        assertEquals(expected,actual);

    }
}