import static org.junit.jupiter.api.Assertions.*;

class intToStrTest {

    @org.junit.jupiter.api.Test
    void normal() throws Exception {
        int Mas = 8;
        String actual=z36.Convert(Mas);
        String expected="8";
        assertEquals(expected,actual);
    }

    @org.junit.jupiter.api.Test
    void minus() throws Exception {
        int Mas = -855555;
        String actual=z36.Convert(Mas);
        String expected="-855555";
        assertEquals(expected,actual);
    }

    @org.junit.jupiter.api.Test
    void action() throws Exception {
        int Mas = 100-98;
        String actual=z36.Convert(Mas);
        String expected="2";
        assertEquals(expected,actual);
    }
}